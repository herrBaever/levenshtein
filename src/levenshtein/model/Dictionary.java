package levenshtein.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Dictionary implements Serializable
{
	private static ArrayList<Dictionary> dictionaryList = new ArrayList<Dictionary>();
	private static final long serialVersionUID = -4642432865786805918L;
	private static final String SAVE_FILE = "dictionaries.dat";
	
	File dictionaryFile;
	String name;
	String illegalLetters;
	Map<String, Set<String>> dictionaryMap;
	
	public Dictionary(File dictionaryFile)
	{
		illegalLetters = "";
		this.dictionaryFile = dictionaryFile;
		dictionaryMap = new HashMap<String, Set<String>>();
		loadRawDictionaryText(dictionaryFile);
		findNeighbours();
	}
	
	public void updateNeighbours()
	{
		long timer = System.currentTimeMillis();
		for (String key : dictionaryMap.keySet())
		{
			dictionaryMap.put(key, new HashSet<String>());
		}
		System.out.println("Timer: updateNeighbours: " + (System.currentTimeMillis() - timer) + " ms");
		findNeighbours();
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dictionary other = (Dictionary) obj;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	private void loadRawDictionaryText(File dictionaryFile)
	{
		Scanner dicReader;
		try 
		{
			dicReader = new Scanner(dictionaryFile);
			long timer = System.currentTimeMillis();
			// puts all the words in the file into the neighbours Map with a new Set as value
			while (dicReader.hasNext())
			{
				dictionaryMap.put(dicReader.next().toLowerCase(), new HashSet<String>());
			}
			System.out.println("Timer: loadDictionary: " + (System.currentTimeMillis() - timer) + " ms");
		} 
		catch (FileNotFoundException e)
		{
			System.out.println("File not found");
		}
	}
	
	public void findNeighbours()
	{
		long timer = System.currentTimeMillis();
		int operation = 0;
		Character[] letterRange = uniqueLetters(dictionaryMap.keySet());
		Set<String> wordsToCheck = new HashSet<String>();
		for (String word: dictionaryMap.keySet())
		{
			wordsToCheck.clear();
			for (int i = 0; i<word.length(); i++)
			{
				// Deletion
				wordsToCheck.add(word.substring(0, i) + word.substring(i+1, word.length()));
				// Substitution and insertion
				for (Character letter: letterRange)
				{
					if (letter != word.charAt(i))	// If this were to be true, we would be adding the word as its own neighbour
					{
						wordsToCheck.add(word.substring(0, i) + letter + word.substring(i+1, word.length()));	// substituting letter at position i in the string
						operation ++;
					}
					wordsToCheck.add(word.substring(0, i) + letter + word.substring(i, word.length()));	// inserting letter at position i in the string			
				}	
			}
			for (String maybeWord: wordsToCheck)	// For all combinations of words now added to the wordsToCheck Set
			{
				if (dictionaryMap.keySet().contains(maybeWord))	// If this word is contained somewhere in the dictionary
				{
					dictionaryMap.get(word).add(maybeWord);	// add this word to the set of the original dictionary entry
				}
			}
		}
		System.out.println("Timer: findNeighbours: " + (System.currentTimeMillis() - timer) + " ms");
		System.out.println("findNeighbours operations: " + operation);
	}
	
	// Might be overkill: finds the letter range of a dictionary and removes illegal letters (return type: char[] better?)
	private Character[] uniqueLetters(Set<String> dictionary)
	{
		int operations = 0;
		long timer = System.currentTimeMillis();
		Set<Character> uniqueLetters = new HashSet<Character>();
		for (String word: dictionary)
		{
			for (int i = 0; i<word.length(); i++)
			{
				uniqueLetters.add(word.charAt(i));
				operations++;
			}
		}
		// Removes illegal letters from the letter range
		for (int i=0; i<illegalLetters.length(); i++)
		{
			Character illegal = illegalLetters.charAt(i);
			uniqueLetters.remove(illegal);
		}

		Set<Character> sortedUniqueLetters = new TreeSet<Character>(uniqueLetters);	// More cpu efficient than just adding to a TreeSet to begin with
		System.out.println("uniqueLetters operations: " + operations);
		System.out.println("Timer: uniqueLetters: " + (System.currentTimeMillis() - timer) + " ms");
		System.out.println("Range of letters: "+sortedUniqueLetters);
		return sortedUniqueLetters.toArray(new Character[sortedUniqueLetters.size()]);
	}

	public void saveDictionary()
	{
		dictionaryList.add(this);
		updateLocalStorage();
	}
	
	public void deleteDictionary()
	{
		dictionaryList.remove(this);
		updateLocalStorage();
	}
	
	private void updateLocalStorage()
	{
		try
		{
			FileOutputStream outFile = new FileOutputStream(SAVE_FILE);
			ObjectOutputStream outObject = new ObjectOutputStream(outFile);
			outObject.writeObject(dictionaryList);
			outFile.close();
			outObject.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")	// Due to the type erasure of deserialization, the ObjectIputStream will produce an Object of type Object
	public static ArrayList<Dictionary> loadDictionariesFromFile()
	{
		try
		{
			FileInputStream inFile = new FileInputStream(SAVE_FILE);
			ObjectInputStream inObject = new ObjectInputStream(inFile);
			dictionaryList = (ArrayList<Dictionary>) inObject.readObject();
			inFile.close();
			inObject.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		return dictionaryList;
	}
	
	public int largestNeighbourSet()
	{
    	int size = 0;
    	for (String set: dictionaryMap.keySet())
    	{
    		if (dictionaryMap.get(set).size() > size)
    		{
    			size = dictionaryMap.get(set).size();
    		}
    	}
    	return size;
	}
	
	@Override
	public String toString()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getIllegalLetters()
	{
		return illegalLetters;
	}

	public Map<String, Set<String>> getDictionaryMap()
	{
		return dictionaryMap;
	}

	public void setIllegalLetters(String illegalLetters)
	{
		this.illegalLetters = illegalLetters;
	}

}
