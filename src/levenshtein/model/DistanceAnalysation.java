package levenshtein.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DistanceAnalysation
{
	private String startWord;
	private String endWord;
	private Map<String, Set<String>> dictionary;
	private int calculatedDistance;
	private String message;
	
	private void findDistance()
	{
		String noPath = ("No path between [" + startWord + "] and [" + endWord + "]" + "\n");
		boolean startContained = dictionary.containsKey(startWord);
		boolean endContained = dictionary.containsKey(endWord);
		if (!startContained && !endContained)
		{
			message = noPath + (startWord + " and " + endWord + " is not contained in set.");
			return;
		}
		else if(!startContained)
		{			
			message = noPath + (startWord + " is not contained in set.");
			return;
		}
		else if(!endContained)
		{			
			message = noPath + (endWord + " is not contained in set.");
			return;
		}
		long timer = System.currentTimeMillis();
		int tier = 0;
		Set<String> checkList = new HashSet<String>(Arrays.asList(startWord));
		Set<String> nextTierPrep = new HashSet<String>();
		Set<String> checkedWordFilter = new HashSet<String>();
		while (true)
		{
			System.out.println("Looking through tier " + tier + "   TierSize: " + checkList.size());
			for (String isItEndWord : checkList)
			{
				if (isItEndWord.equals(endWord))
				{
				    System.out.println("\nSize of Dictionary: " + dictionary.size() + " word entries");
					System.out.println("Timer: findDistance: " + (System.currentTimeMillis() - timer) + " ms");
					calculatedDistance = tier;
					message = "Levenshtein distance between [" + startWord + "] and [" + endWord + "] : " + tier;
					return;
				}
				checkedWordFilter.add(isItEndWord);
				nextTierPrep.addAll(dictionary.get(isItEndWord));
			}
			nextTierPrep.removeAll(checkedWordFilter);	// To filter out words already checked
			if (nextTierPrep.isEmpty())
			{
				System.out.println("Timer: findDistance: " + (System.currentTimeMillis() - timer) + " ms");
				message = noPath;
				return;
			}
			tier++;
			checkList.clear();
			checkList.addAll(nextTierPrep);
			nextTierPrep.clear();
		}
	}

	public int getCalculatedDistance()
	{
		return calculatedDistance;
	}

	public String getMessage()
	{
		return message;
	}

	public DistanceAnalysation(String startWord, String endWord, Map<String, Set<String>> dictionary)
	{
		this.startWord = startWord;
		this.endWord = endWord;
		this.dictionary = dictionary;
		findDistance();
	}
}
