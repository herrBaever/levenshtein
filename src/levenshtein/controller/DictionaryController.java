package levenshtein.controller;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import levenshtein.model.Dictionary;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class DictionaryController implements Initializable
{
	private ObservableList<Dictionary> data = FXCollections.observableArrayList();
	private Dictionary currentDic;
	private Stage namePopupStage;
	private Scene nameScene;
	private Scene deleteDialogScene;
	private Stage deleteDialogStage;

	@FXML
	private ChoiceBox<Dictionary> dictionaryChoiceBox = new ChoiceBox<Dictionary>();

	@FXML
	private Button loadRawDictionary;

	@FXML
	private TextField ignoredChars;
	
	@FXML
	private CheckBox charIgnoreEdit;
	
    @FXML
    private Label ignoreLabel;
    
	// delete dialog
	@FXML
	private YesNoDialogController deleteDialogController;

	@FXML
	private AnchorPane deleteDialog;

	// name dialog
	@FXML
	private DictNamerPopupController newDictFileNamePopupController;

	@FXML
	private AnchorPane newDictFileNamePopup;
	
    
	@FXML
	public void fireNewDictionary(ActionEvent event) 
	{
		Stage fileStage = new Stage();
		fileStage.setTitle("Choose File");
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().add
		(
				new FileChooser.ExtensionFilter("Txt", "*.txt")
		);
		File filePath = fileChooser.showOpenDialog(fileStage);
		if(filePath != null)
		{
			System.out.println("at fireNewDic" +filePath);
			currentDic = new Dictionary(filePath);
			namerPopup();
		}
	}

	private void namerPopup()
	{
		namePopupStage = new Stage();
		namePopupStage.setTitle("Choose name");
		namePopupStage.setScene(nameScene);
		namePopupStage.show();

		newDictFileNamePopupController.getNewDictNameOkay().setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent event)
			{
				currentDic.setName(newDictFileNamePopupController.getNewDictName().getText());
				data.add(currentDic);
				dictionaryChoiceBox.setItems(data);
				dictionaryChoiceBox.getSelectionModel().selectFirst();
				namePopupStage.close();
				newDictFileNamePopupController.getNewDictName().setText("");
			}
		});

		newDictFileNamePopupController.getNewDictNameCancel().setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent event)
			{
				namePopupStage.close();
				newDictFileNamePopupController.getNewDictName().setText("");
			}

		});
	}
	
	@FXML
	public void fireSaveRawDictionary(ActionEvent event)
	{
		dictionaryChoiceBox.getValue().saveDictionary();
	}

	@FXML
	private void fireLoadDictionary()
	{
		for (Dictionary dict: Dictionary.loadDictionariesFromFile())
		{
			if (!data.contains(dict))
			{
				data.add(dict);
			}
		}
		dictionaryChoiceBox.setItems(data);
		dictionaryChoiceBox.getSelectionModel().selectFirst();
	}

	public void fireDeleteDictionary()
	{
		if (!dictionaryChoiceBox.getSelectionModel().isEmpty())	// if some dictionary is selected in the choicebox
		{
			currentDic = dictionaryChoiceBox.getValue();
			deleteDialogController.getDictNameLabel().setText(currentDic.toString());
			deleteDialogStage = new Stage();
			deleteDialogStage.setTitle("Delete Dictionary");
			deleteDialogStage.setScene(deleteDialogScene);
			deleteDialogStage.show();

			deleteDialogController.getYesButton().setOnAction(new EventHandler<ActionEvent>()
					{	
				@Override
				public void handle(ActionEvent event)
				{
					currentDic.deleteDictionary();
					data.remove(currentDic);
					dictionaryChoiceBox.getSelectionModel().selectFirst();
					deleteDialogStage.close();
				}
					});

			deleteDialogController.getNoButton().setOnAction(new EventHandler<ActionEvent>()
					{
				@Override
				public void handle(ActionEvent event)
				{
					deleteDialogStage.close();
				}
					});

		}
	}


    @FXML
    void charIgnoreFieldClick(MouseEvent event) 
    {
    	if (event.getClickCount() == 2)		// checking for double click on textfield
    	{
    		charIgnoreEdit.setSelected(true);
    		fireCharIgnoreEdit();
    	}
    }
    
    @FXML
    void fireCharIgnoreEdit() 
    {
    	if (charIgnoreEdit.isSelected())
    	{
    		ignoredChars.setEditable(true);
    		ignoredChars.setStyle(null);
    	}
    	else
    	{
    		ignoredChars.setEditable(false);
    		ignoredChars.setStyle("-fx-background-color: lightgrey;");
    		dictionaryChoiceBox.getValue().setIllegalLetters(ignoredChars.getText());
    		dictionaryChoiceBox.getValue().updateNeighbours();
    	}
    }

	private void setIgnoreVisibility(boolean igno)
	{
		ignoredChars.setVisible(igno);
		charIgnoreEdit.setVisible(igno);
		ignoreLabel.setVisible(igno);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1)
	{
		MainApplication.setDictionaryController(this);
		ignoredChars.setEditable(false);
		ignoredChars.setStyle("-fx-background-color: lightgrey;");	// http://docs.oracle.com/javafx/2/css_tutorial/jfxpub-css_tutorial.htm
		nameScene = new Scene(newDictFileNamePopup);
		deleteDialogScene = new Scene(deleteDialog);
		dictionaryChoiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Dictionary>()
		{
			@Override
			public void changed(ObservableValue<? extends Dictionary> observable, Dictionary oldValue, Dictionary newValue)
			{
				setIgnoreVisibility(newValue != null);
				if (newValue != null && !newValue.equals(oldValue))
				{
					ignoredChars.setText(newValue.getIllegalLetters());
				}
			}
		});
				
		setIgnoreVisibility(false);
		fireLoadDictionary();
	}

	protected ChoiceBox<Dictionary> getDictionaryChoiceBox()
	{
		return dictionaryChoiceBox;
	}
	
}
