package levenshtein.controller;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class MainApplication extends Application {

    private FXMLLoader loader = null;
	private Stage primaryStage = null;
	private AnchorPane rootLayout = null;
    private Scene scene = null;
	private static DictionaryController dictionaryController;
	private static DistanceController distanceController;

	@Override
	public void start(Stage primaryStage) 
	{
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Levenshtein");
		try
		{
            System.out.println("FXML resource: " + MainApplication.class.getResource("MainView.fxml"));
			loader = new FXMLLoader(MainApplication.class.getResource("MainView.fxml"));
			rootLayout = (AnchorPane) loader.load();
			scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args) 
	{
		launch(args);
	}


	public Stage getPrimaryStage()
	{
		return primaryStage;
	}

	protected static DictionaryController getDictionaryController()
	{
		return dictionaryController;
	}

	protected static DistanceController getDistanceController()
	{
		return distanceController;
	}

	protected static void setDictionaryController(DictionaryController dictionaryController)
	{
		MainApplication.dictionaryController = dictionaryController;
	}

	protected static void setDistanceController(DistanceController distanceController)
	{
		MainApplication.distanceController = distanceController;
	}

}
