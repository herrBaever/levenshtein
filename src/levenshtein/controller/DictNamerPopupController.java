package levenshtein.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class DictNamerPopupController implements Initializable
{

    @FXML
    private TextField newDictName;
    
	@FXML
	private Button newDictNameOkay;
	
	@FXML
	private Button newDictNameCancel;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1)
	{

	}
	
	
	protected TextField getNewDictName()
	{
		return newDictName;
	}


	protected Button getNewDictNameOkay()
	{
		return newDictNameOkay;
	}


	protected Button getNewDictNameCancel()
	{
		return newDictNameCancel;
	}

}
