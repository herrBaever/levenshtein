package levenshtein.controller;

import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import levenshtein.model.Dictionary;

public class NeighbourController implements Initializable
{

	private Dictionary currentDic;
	private Set<String> neighbourSet;
	
	@FXML
	private GridPane neighbourButtonArea;
	
    @FXML
    private TextField wordField;

    @FXML
    private void fireGoNeighbours()
    {
    	currentDic = MainApplication.getDictionaryController().getDictionaryChoiceBox().getValue();
    	if (!currentDic.getDictionaryMap().containsKey(wordField.getText()))
    	{
    		System.out.println(wordField.getText()+ " is not contained in the dictionary");
    		return;
    	}
    	neighbourButtonArea.getChildren().clear();
    	neighbourSet = currentDic.getDictionaryMap().get(wordField.getText());
    	System.out.println(neighbourSet);
    	populateNeighbourButtons();
    }
    
    private void populateNeighbourButtons()
    {
    	int index = 0;
    	for (String neighbourWord: neighbourSet)
    	{
    		final Button word = new Button(neighbourWord);
    		neighbourButtonArea.add(word, index%4, index/4);	// Filling 0,1,2,3 column in a row, then move on to next row
    		word.setOnAction(new EventHandler<ActionEvent>()
    				{
    			@Override
    			public void handle(ActionEvent event)
    			{
    				wordField.setText(word.getText());
    				fireGoNeighbours();
    			}
    				});

    		index++;
    	}
    }
    
	@Override
	public void initialize(URL arg0, ResourceBundle arg1)
	{
		neighbourSet = new HashSet<String>();
	}

}
