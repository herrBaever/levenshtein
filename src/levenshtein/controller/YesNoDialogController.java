package levenshtein.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class YesNoDialogController implements Initializable
{
	@FXML
    private Label dictNameLabel;

    @FXML
    private Button noButton;

    @FXML
    private Button yesButton;
    
	@Override
	public void initialize(URL arg0, ResourceBundle arg1)
	{
		
	}

	protected Button getNoButton()
	{
		return noButton;
	}

	protected Button getYesButton()
	{
		return yesButton;
	}


	protected Label getDictNameLabel()
	{
		return dictNameLabel;
	}
}
