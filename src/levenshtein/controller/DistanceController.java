package levenshtein.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import levenshtein.model.Dictionary;
import levenshtein.model.DistanceAnalysation;

public class DistanceController implements Initializable
{
	
	private Dictionary currentDic;
	
	@FXML 
	private AnchorPane distAnchor;
	
    @FXML
    private Label resultLabel;

    @FXML
    private Label resultTitelLabel;
    
    @FXML
    private TextField fieldOne;

    @FXML
    private TextField fieldTwo;
    
    @FXML
    private void calculateDist() 
    {
    	currentDic = MainApplication.getDictionaryController().getDictionaryChoiceBox().getValue();
    	if (currentDic != null)
    	{
    		DistanceAnalysation dist = new DistanceAnalysation(fieldOne.getText(), fieldTwo.getText(), currentDic.getDictionaryMap());
    		System.out.println(dist.getMessage());
    		resultTitelLabel.setStyle("-fx-text-fill: hotpink; -fx-font-size: 18px; -fx-font-weight: bold; -fx-font-family: fantasy;");
    		resultTitelLabel.setVisible(true);
//    		Integer distance = dist.getCalculatedDistance();
//    		resultLabel.setText(distance.toString()); 
    		resultLabel.setText((new Integer(dist.getCalculatedDistance()).toString()));
    		resultLabel.setStyle("-fx-text-fill: turquoise; -fx-font-size: 50px; -fx-font-weight: bolder;");
    		distAnchor.setStyle("-fx-background-color: darkviolet;");
    	}
    }
    
    public DistanceController()
    {
    	
    }

	@Override
	public void initialize(URL arg0, ResourceBundle arg1)
	{
		MainApplication.setDistanceController(this);
		resultTitelLabel.setVisible(false);
	}
	
	protected void setDictionary(Dictionary dic)
	{
		currentDic = dic;
	}
	
}