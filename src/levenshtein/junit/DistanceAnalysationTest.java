package levenshtein.junit;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import levenshtein.model.DistanceAnalysation;

import org.junit.Before;
import org.junit.Test;

public class DistanceAnalysationTest
{
	Map<String, Set<String>> dictionary = new HashMap<String, Set<String>>();
	DistanceAnalysation catToHat;
	DistanceAnalysation hatToCoat;

	@Before
	public void setUp()
	{
		dictionary.put("cat", new HashSet<String>(Arrays.asList("hat","coat")));
		dictionary.put("hat", new HashSet<String>(Arrays.asList("cat")));
		dictionary.put("coat", new HashSet<String>(Arrays.asList("cat")));
		catToHat = new DistanceAnalysation("cat", "hat", dictionary);
		hatToCoat = new DistanceAnalysation("hat", "coat", dictionary);
	}

	@Test
	public void testGetCalculatedDistance()
	{
		// There should be 1 levenshtein edit distance between "cat" and "hat"
		int expResult = 1;
		assertEquals(expResult, catToHat.getCalculatedDistance());

		// There should be 2 levenshtein edit distance between "hat" and "coat"
		expResult = 2;
		assertEquals(expResult, hatToCoat.getCalculatedDistance());
	}

	@Test
	public void testGetMessage()
	{
		// for catToHat
		String expResult ="Levenshtein distance between [cat] and [hat] : 1";
		assertEquals(expResult, catToHat.getMessage());
		
		// for hatToCoat
		expResult ="Levenshtein distance between [hat] and [coat] : 2";
		assertEquals(expResult, hatToCoat.getMessage());
	}
}
